import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '../views/HomePage.vue'
import AboutPage from '../views/AboutPage.vue'
import PortfolioPage from '../views/PortfolioPage.vue'
import ContactPage from '../views/ContactPage.vue'
import ExperiencePage from "../views/ExperiencePage.vue";

const routes = [
    { path: '/cv', component: HomePage },
    { path: '/cv/about', component: AboutPage },
    { path: '/cv/portfolio', component: PortfolioPage },
    { path: '/cv/contact', component: ContactPage },
    { path: '/cv/experience', component: ExperiencePage}
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
